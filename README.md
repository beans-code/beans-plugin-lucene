# Lucene plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`Lucene` is an open-source search software [https://lucene.apache.org](https://lucene.apache.org).
