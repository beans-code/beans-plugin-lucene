package net.beanscode.plugin.lucene;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.rendersnake.HtmlCanvas;

import net.beanscode.model.connectors.LuceneConnector;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.lucene.LuceneDatabaseProvider;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.lucene.LuceneSearchEngineProvider;

public class LucenePlugin implements Plugin {

	public LucenePlugin() {
		
	}

	@Override
	public String getName() {
		return "Lucene plugin";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}

	@Override
	public String getDescription() {
		return "Lucene plugin provides: "
				+ "1. Lucene search engine - embedded search engine, ideal for standalone use;"
				+ "2. Lucene database provider - allows to store BEANS data on Lucene search engine (without a need to "
				+ "install any database server). This database is suitable for small databases, which in "
				+ "principle fit into your local hard drive. "
				+ "3. Lucene connector - allows to store results of scripts into Lucene search index, which "
				+ "makes the data indexed by default";
	}

	@Override
	public String getPanel(UUID userId) throws IOException {
		return new HtmlCanvas()
				.h3()
					.content("Description")
				.p()
					.content(getDescription()).toHtml();
	}

	@Override
	public List<Func> getFuncList() {
		return null;
	}

	@Override
	public List<Class<? extends Connector>> getConnectorClasses() {
		List<Class<? extends Connector>> conn = new ArrayList<Class<? extends Connector>>();
		conn.add(LuceneConnector.class);
		return conn;
	}

	@Override
	public List<Class<? extends NotebookEntry>> getNotebookEntries() {
		return null;
	}
	
	@Override
	public List<Class<? extends DatabaseProvider>> getDatabaseProviderList() {
		List<Class<? extends DatabaseProvider>> db = new ArrayList<>();
		db.add(LuceneDatabaseProvider.class);
		return db;
	}
	
	@Override
	public List<Class<? extends SearchEngineProvider>> getSearchEngineProviderList() {
		List<Class<? extends SearchEngineProvider>> searchEngines = new ArrayList<>();
		searchEngines.add(LuceneSearchEngineProvider.class);
		return searchEngines;
	}
	
	@Override
	public boolean selfTest(UUID userId, OutputStreamWriter output) throws ValidationException, IOException {
		output.write("TODO");
		return true;
	}
}
